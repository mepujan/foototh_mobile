class EmployeeModel {
 final String name;
 final String address;
 final String phoneNumber;
  final String mobileNumber;
  final int hotelID;
  final int postID;

  save() {
    print('Saving employee detail to server');
  }
  EmployeeModel({this.name,this.address,this.phoneNumber,this.mobileNumber,this.hotelID,this.postID});
  factory EmployeeModel.fromJson(Map<String,dynamic> json){
    return EmployeeModel(
      name:json['name'],
      address: json['address'],
      phoneNumber: json['phoneNumber'],
      mobileNumber: json['mobileNumber'],
      hotelID: json['hotelID'],
      postID: json['postID']
    );

  }
}
