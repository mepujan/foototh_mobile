import 'package:flutter/material.dart';
import 'package:foototh/screens/splashscreen.dart';

void main()=>runApp(Foototh());

class Foototh extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),

    );
  }
}
