import 'package:flutter/material.dart';
import 'package:flag/flag.dart';
import 'package:foototh/screens/signin_mobile.dart';

class LanguageSelection extends StatefulWidget {
  @override
  _LanguageSelectionState createState() => _LanguageSelectionState();
}

class _LanguageSelectionState extends State<LanguageSelection> {
  bool _isChecked1 = true;
  bool _isChecked2 = false;

  var _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Form(
        key: _formKey,
        child: new ListView(
          children: <Widget>[
            new Container(
              margin: new EdgeInsets.all(50.0),
            ),
            Image.asset(
              'images/foototh-png.png',
              width: 300,
              height: 200,
            ), 
            new Row(
              children: <Widget>[
                new Expanded(
                  child: new Text(  
                    'Select Language',
                    textAlign: TextAlign.center,
                    style: new TextStyle(fontSize: 25.0),
                  ),
                )
              ],
            ),
             new Padding(
            padding: new EdgeInsets.all(5.0),
          ),
            new Padding(
              padding: EdgeInsets.all(5.0),
              child: new Row(
                children: <Widget>[
                  new Expanded(
                    child: Flags.getMiniFlag('US', 20, null),
                  ),
                  new Expanded(
                    child:new SizedBox(
                    width: 500.0,
                    child: new RaisedButton(
                      child: new Text('ENGLISH', textScaleFactor: 1.5,style: new TextStyle(fontSize: 10.0),),
                      onPressed: () {
                        setState(() {
                          _isChecked1 = true;
                          _isChecked2 = false;
                          debugPrint('english is selected');
                        });
                      },
                    ),
                    ),
                  ),
                  new Expanded(
                    child: new CheckboxListTile(
                      value: _isChecked1,
                      controlAffinity: ListTileControlAffinity.leading,
                      onChanged: (val) {
                      },
                      activeColor: Colors.deepOrange,
                    ),
                  )
                ],
              ),
            ),
            new Padding(
              padding: EdgeInsets.all(5.0),
              child: new Row(
                children: <Widget>[
                  new Expanded(
                    child: Flags.getMiniFlag('NP', 20, null),
                  ),
                  new Expanded(
                    child: new RaisedButton(
                      child: new Text('NEPALI', textScaleFactor: 1.5,style: new TextStyle(fontSize: 10.0),),
                      onPressed: () {
                        setState(() {
                          _isChecked2 = true;
                          _isChecked1 = false;
                          debugPrint('Nepali is selected');
                        });
                      },
                      
                    ),
                  ),
                  new Expanded(
                    child: new CheckboxListTile(
                    controlAffinity: ListTileControlAffinity.leading,
                    value: _isChecked2,
                    onChanged: (val){
                      if(val==false){
                        return 'Please choose any one language.';
                      }
                      return null;
                    },
                    activeColor: Colors.deepOrange,
                  ))
                ],
              ),
            ),
            new Padding(
              padding: EdgeInsets.all(5.0),
              child: new RaisedButton(
                  child: new Text('Next', textScaleFactor: 1.5),
                  color: Colors.deepOrange,
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      setState(() {
                        _loginPageNavigator();
                        debugPrint('Next button is selected');
                      });
                    }
                  }),
            )
          ],
        ),
      ),
    );
  }

  void _loginPageNavigator() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Signinviamobile()));
  }
}
