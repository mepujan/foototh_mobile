import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:foototh/screens/signin_mobile.dart';
import 'package:http/http.dart' as http;
import 'package:foototh/Pages/hotel_admin.dart';

class LoginEmployee extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginEmployee> {
  var _formKey = GlobalKey<FormState>();
  bool _isVisible = false;
  TextEditingController userNameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = Theme.of(context).textTheme.title;
    return new Scaffold(
      persistentFooterButtons: <Widget>[
        BackButton(
          color: Colors.deepOrange,
          onPressed: (){
            setState(() {
              _userLogin();
            });
          },
        ),
      ],
      body: new Form(
          key: _formKey,
          child: new ListView(
            children: <Widget>[
              new Container(
                margin: EdgeInsets.all(50.0),
              ),
              Image.asset(
                'images/foototh-png.png',
                width: 300,
                height: 200,
              ),
              new Padding(
                padding: EdgeInsets.all(5.0),
                child: TextFormField(
                  controller: userNameController,
                  keyboardType: TextInputType.emailAddress,
                  style: textStyle,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please Enter Your Email.';
                    } else {
                      if (isEmail(userNameController.text) == false) {
                        return 'Please check your Email.';
                      }

                      return null;
                    }
                  },
                  decoration: InputDecoration(
                      labelText: 'Enter your Email.',
                      labelStyle: textStyle,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0))),
                ),
              ),
              new Padding(
                padding: new EdgeInsets.only(left: 10.0),
                child: new Visibility(
                  child: Text('Please Enter the valid email.',
                      style: TextStyle(color: Colors.red)),
                  visible: _isVisible,
                ),
              ),
              new Padding(
                padding: EdgeInsets.all(5.0),
                child: TextFormField(
                  controller: passwordController,
                  obscureText: true,
                  style: textStyle,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please Enter Password.';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                      labelText: 'Enter your Password',
                      labelStyle: textStyle,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                      )),
                ),
              ),
              new Padding(
                padding: new EdgeInsets.only(left: 10.0),
                child: new Visibility(
                  child: Text('Please Enter the valid password.',
                      style: TextStyle(color: Colors.red)),
                  visible: _isVisible,
                ),
              ),
              new Padding(
                padding: EdgeInsets.all(5.0),
                child: new RaisedButton(
                  color: Colors.deepOrange,
                  textColor: Theme.of(context).primaryColorLight,
                  child: Text(
                    'Login',
                    textScaleFactor: 1.5,
                  ),
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      setState(() {
                        _login(
                            userNameController.text, passwordController.text);
                        debugPrint('login is pressed');
                      });
                    }
                  },
                ),
              ),
            ],
          )),
    );
  }

  void _toggle() {
    setState(() {
      _isVisible = true;
    });
  }

  bool isEmail(String em) {
    String p =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    RegExp regExp = new RegExp(p);

    return regExp.hasMatch(em);
  }

  void _login(String username, String password) async {
    Map data = {'email': username, 'password': password};

    var response = await http.post(
        Uri.encodeFull("http://10.0.3.2:8000/rest-auth/login/"),
        headers: {'Accept': 'application/json'},
        body: data);
    if (response.statusCode == 200) {
      print(response.body);
      json.decode(response.body);
      setState(() {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => HotelAdminPage()));
      });
    } else {
      _toggle();
      print(response.body);
    }
  }
  void _userLogin() async{
    setState(() {
       Navigator.push(context, MaterialPageRoute(builder: (context) => Signinviamobile()));
    });
  }
}
