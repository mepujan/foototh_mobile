import 'package:flutter/material.dart';
import 'package:foototh/screens/languageSelection.dart';
import 'dart:async';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {


  startTime() async {
    var _duration = new Duration(seconds: 3);
    return new Timer(_duration, navigationPage);
  }
  void navigationPage() {
    Navigator.push(context, MaterialPageRoute(builder: (context)=>LanguageSelection()));
  }
  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: new Image.asset(
          'images/foototh-png.png',
          width: 300,
          height: 250,
        ),
      ),
    );
  }
}