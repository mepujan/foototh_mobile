import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:foototh/Pages/menu.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as JSON;
import 'package:foototh/screens/staffLogin.dart';
import 'languageSelection.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';

final GoogleSignIn _googleSignIn = GoogleSignIn(
  scopes: [
    'email',
    'https://www.googleapis.com/auth/contacts.readonly',
  ],
);

class Signinviamobile extends StatefulWidget {
  
  @override
  _SigninviamobileState createState() => _SigninviamobileState();
}

class _SigninviamobileState extends State<Signinviamobile> {
  String barcode = "";
  _loginWithGoogle() async {
    try {
      final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication;
      print(googleAuth.accessToken);
      setState(() {
        _isLoggedIn = true;
          // Navigator.push(
          //   context, MaterialPageRoute(builder: (context) =>ScanScreen()));
          scan();
        print('Login Successful');
      });
    } catch (err) {
      print(err);
    }
  }

  bool _isLoggedIn = false;
  Map userProfile;
  final facebookLogin = FacebookLogin();

  _loginWithFB() async {
    final result = await facebookLogin.logInWithReadPermissions(['email']);
    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        final graphResponse = await http.get(
            'https://graph.facebook.com/v2.12/me?fields=name,picture,email&access_token=${result.accessToken.token}');
        final profile = JSON.jsonDecode(graphResponse.body);
        print(profile);
        setState(() {
          userProfile = profile;
          _isLoggedIn = true;
          scan();
            // Navigator.push(
            // context, MaterialPageRoute(builder: (context) => ScanScreen()));
        });
        break;

      case FacebookLoginStatus.cancelledByUser:
        setState(() => _isLoggedIn = false);
        break;
      case FacebookLoginStatus.error:
        setState(() => _isLoggedIn = false);
        break;
    }
  }

  var _formKey = GlobalKey<FormState>();
  TextEditingController mobilenumberController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = Theme.of(context).textTheme.title;
    return new Scaffold(
        persistentFooterButtons: <Widget>[
        BackButton(
          color: Colors.deepOrange,
          onPressed:(){
            setState(() {
              _signInViaMobile();
            });
          }
        ),
      ],
        body: new Form(
      key: _formKey,
      child: new ListView(
        children: <Widget>[
          new Container(
            margin: EdgeInsets.all(50.0),
          ),
          Image.asset(
            'images/foototh-png.png',
            width: 300,
            height: 200,
          ),
          new Row(
            children: <Widget>[
              new Expanded(
                child: new Text(
                  'SignIn with Mobile',
                  textAlign: TextAlign.center,
                  style: new TextStyle(fontSize: 25.0),
                ),
              )
            ],
          ),
          new Padding(
            padding: new EdgeInsets.all(5.0),
          ),
          new Padding(
              padding: new EdgeInsets.all(5.0),
              child: new TextFormField(
                controller: mobilenumberController,
                style: textStyle,
                keyboardType: TextInputType.phone,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter your mobilenumber.';
                  }
                  return null;
                },
                decoration: new InputDecoration(
                    labelText: 'Enter your mobile number.',
                    border: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0))),
              )),
          new Padding(
            padding: EdgeInsets.all(5.0),
            child: new RaisedButton(
              child: Text(
                'Get OTP',
                textScaleFactor: 1.5,
              ),
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  setState(() {
                    debugPrint('otp button is pressed.');
                  });
                }
              },
              color: Colors.deepOrange,
            ),
          ),
          new Padding(
            padding: EdgeInsets.all(5.0),
            child: new Row(
              children: <Widget>[
                new Expanded(
                    child: SignInButton(
                  Buttons.Facebook,
                  onPressed: () {
                    setState(() {
                      _loginWithFB();
                    });
                  },
                )),
                new Expanded(
                    child: SignInButton(
                  Buttons.Google,
                  onPressed: () {
                    setState(() {
                      _loginWithGoogle();
                    });
                  },
                )),
              ],
            ),
          ),
          new Padding(
            padding: new EdgeInsets.all(30.0),
            child: new Center(
              child: new InkWell(
                child: new Text('Login as Staff ?'),
                onTap: () => Navigator.push(context,
                    MaterialPageRoute(builder: (context) => LoginEmployee())),
              ),
            ),
          )
        ],
      ),
    ));
  }
  void _signInViaMobile() async{
    setState(() {
      Navigator.push(context,MaterialPageRoute(builder: (context) => LanguageSelection()));
    });
  }


 void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Scanned Text"),
          content: Text(this.barcode),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Ok"),
              onPressed: () {
                Navigator.push(
            context, MaterialPageRoute(builder: (context) => MenuPage()));
              },
            ),
          ],
        );
      },
    );
  }

  Future scan() async {
    try {
      String barcode = await BarcodeScanner.scan();
      setState(() => this.barcode = barcode);
      _showDialog();
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          this.barcode = 'The user did not grant the camera permission!';
        });
      } else {
        setState(() => this.barcode = 'Unknown error: $e');
      }
    } on FormatException {
      setState(() => this.barcode =
          'null (User returned using the "back"-button before scanning anything. Result)');
    } catch (e) {
      setState(() => this.barcode = 'Unknown error: $e');
    }
  }


}
