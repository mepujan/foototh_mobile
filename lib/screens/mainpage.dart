import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:foototh/screens/staffLogin.dart';
import 'package:http/http.dart' as http;

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: Text("Main Page"),
        backgroundColor: Colors.deepOrange,
        actions: <Widget>[
          FlatButton(
            textColor: Colors.white,
            onPressed: () {
              setState(() {
                print('logout button is pressed.');
                _logout();
              });
            },
            child: Text("LOGOUT"),
            shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
          ),
        ],
      ),
      drawer: new Drawer(),
    );
  }
  void _logout() async
  {
    var response = await http.post(
        Uri.encodeFull("http://localhost:8000/rest-auth/logout/"),
        headers: {'Accept': 'application/json'},
        );
        if (response.statusCode == 200) {
        json.decode(response.body);
        print(response.body);
      setState(() {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => LoginEmployee()));
      });
    } else {
      print(response.body);
    }
  }
}
