import 'package:flutter/material.dart';

TextStyle dropUpHintStyle() {
  return TextStyle(
    fontWeight: FontWeight.bold,
    fontStyle: FontStyle.italic,
  );
}

TextStyle appBarTitleTextStyle() {
  return TextStyle(
    color: Colors.white,
    fontSize: 22,
  );
}

TextStyle sortOptionsStyle() {
  return TextStyle(
    color: Colors.black,
    fontWeight: FontWeight.bold,
  );
}

TextStyle sortWidgetLabelStyle() {
  return TextStyle(
    fontWeight: FontWeight.bold,
    fontStyle: FontStyle.italic,
  );
}

TextStyle foodItemPriceStyle() {
  return TextStyle(
    fontSize: 20,
    fontStyle: FontStyle.italic,
  );
}

TextStyle listItemCategoryStyle() {
  return TextStyle(fontSize: 10, fontStyle: FontStyle.italic);
}

TextStyle listItemProductNameStyle() {
  return TextStyle(fontSize: 25, fontWeight: FontWeight.normal);
}

TextStyle bottomBarButtonStyle() {
  return TextStyle(
    fontSize: 30,
    fontWeight: FontWeight.bold,
  );
}

TextStyle listItemQuantityStyle() {
  return TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.bold,
  );
}

TextStyle listItemTableNumberStyle() {
  return TextStyle(
    fontSize: 15,
    fontWeight: FontWeight.bold,
  );
}

TextStyle listItemProductNameStyle2() {
  return TextStyle(fontSize: 21, fontWeight: FontWeight.normal);
}

TextStyle drawerTitleTextStyle() {
  return TextStyle(
    color: Colors.deepOrange,
    fontSize: 35,
  );
}

TextStyle drawerNormalTextStyle() {
  return TextStyle(
    color: Colors.deepOrange,
    fontSize: 16,
  );
}

TextStyle drawerButtonTextStyle() {
  return TextStyle(
    color: Colors.white,
    fontSize: 20,
  );
}

TextStyle drawerGrandTotalTextStyle() {
  return TextStyle(
    color: Colors.deepOrange,
    fontSize: 20,
  );
}

TextStyle drawerGrandTotalStyle() {
  return TextStyle(
    color: Colors.deepOrange,
    fontSize: 20,
    fontWeight: FontWeight.bold,
  );
}

TextStyle drawerTotalPriceStyle() {
  return TextStyle(fontStyle: FontStyle.italic);
}

TextStyle paymentPageTitleTextStyle() {
  return TextStyle(
    color: Colors.deepOrange,
    fontSize: 45,
    letterSpacing: 2,
  );
}

TextStyle paymentPageNormalTextStyle() {
  return TextStyle(
    color: Colors.deepOrange,
    fontSize: 30,
  );
}

TextStyle popUpTitleTextStyle() {
  return TextStyle(
    color: Colors.deepOrange,
    fontSize: 30,
  );
}

TextStyle popUpNormalTextStyle() {
  return TextStyle(
    color: Colors.deepOrange,
    fontSize: 16,
  );
}

TextStyle adminDrawerNormalTextStyle() {
  return TextStyle(fontSize: 20);
}

TextStyle addEmployeeFormLabelTextStyle() {
  return TextStyle(fontSize: 18);
}

///////////////////////////////////////////

Widget dividerWidget() {
  return Divider(
    color: Colors.deepOrange,
    thickness: 2,
  );
}

class MySeparator extends StatelessWidget {
  final double height;
  final Color color;

  const MySeparator({this.height = 2, this.color = Colors.deepOrange});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final boxWidth = constraints.constrainWidth();
        final dashWidth = 10.0;
        final dashHeight = height;
        final dashCount = (boxWidth / (2 * dashWidth)).floor();
        return Flex(
          children: List.generate(dashCount, (_) {
            return Padding(
              padding: const EdgeInsets.symmetric(vertical: 8),
              child: SizedBox(
                width: dashWidth,
                height: dashHeight,
                child: DecoratedBox(
                  decoration: BoxDecoration(color: color),
                ),
              ),
            );
          }),
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          direction: Axis.horizontal,
        );
      },
    );
  }
}

///////////////////////////////////////////

InputDecoration beautifulField(label) {
  return InputDecoration(
    labelText: label,
    fillColor: Colors.white,
    border: new OutlineInputBorder(
      borderRadius: new BorderRadius.circular(25.0),
      borderSide: new BorderSide(),
    ),
  );
}
