import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foototh/CustomWidgets/add_employee.dart';
import 'package:foototh/CustomWidgets/admin_drawer.dart';
import 'package:foototh/CustomWidgets/dropup.dart';
import 'package:foototh/CustomWidgets/employee_list.dart';
import 'package:foototh/Styles/style.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class HotelAdminPage extends StatelessWidget {
  // WidgetBuilder(context){
  @override
  Widget build(BuildContext context) {
////////////////////Temporary///////////////////////////
    final String hotelName = 'Hotel';
    final String hotelPhone = '015598765';
    final String hotelAddress = 'Mid-Baneshwor, Lalitpur';
    final String adminName = 'John Doe';
    final String adminMobile = '9841234567';

    ///
    return Scaffold(
      appBar: _appBarWidget(hotelName),
      endDrawer: AdminDrawer(adminName, adminMobile, hotelPhone, hotelAddress, hotelName),
      bottomNavigationBar: _bottomBar(),
      body: EmployeeList(),
      floatingActionButton: _addEmployee(context),
    );
  }
}

Widget _appBarWidget(hotelName) {
  return AppBar(
    backgroundColor: Colors.deepOrange,
    title: _titleWidget(hotelName),
  );
}

Widget _bottomBar() {
  return Container(
      height: 50,
      constraints: BoxConstraints.tightFor(height: 50),
      child: BottomAppBar(
        color: Colors.deepOrangeAccent,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              "Sort Employee By Post:",
              style: sortWidgetLabelStyle(),
            ),
            DropUpWidgetForSortingFood('Post')
          ],
        ),
      ));
}

Widget _titleWidget(hotelName) {
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        _signOutWidget(),
        _titleTextWidget(hotelName),
      ]);
}

Widget _signOutWidget() {
  return Transform.rotate(
    angle: 3.2,
    child: IconButton(
      icon: new Icon(MdiIcons.logout),
      splashColor: Colors.white12,
      color: Colors.white,
      tooltip: 'Sign Out',
      iconSize: 35,
      onPressed: () => print("Pressed"),
    ),
  );
}

Widget _titleTextWidget(hotelName) {
  return Expanded(
    child: Text(
      "$hotelName's Employees",
      style: appBarTitleTextStyle(),
    ),
  );
}

Widget _addEmployee(context) {
  return FloatingActionButton(
    onPressed: () => Navigator.push(context, MaterialPageRoute(
        builder: (context) => AddEmployee()
    )),
    backgroundColor: Colors.deepOrangeAccent,
    splashColor: Colors.white12,
    hoverColor: Colors.deepOrange,
    tooltip: "Add Employee",
    child: Icon(
      Icons.add,
      size: 40,
    ),
  );
}
