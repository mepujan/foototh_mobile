import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:foototh/CustomWidgets/payment_apis.dart';
import 'package:foototh/Styles/style.dart';

class PaymentPage extends StatelessWidget {
  final String hotelName;
  final String tableNumber;
  final String grandTotal;
  const PaymentPage(this.hotelName, this.tableNumber, this.grandTotal);

  // WidgetBuilder(context){
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: paymentAppBarWidget(),
      body: paymentBodyWidget(context, hotelName, tableNumber, grandTotal),
    );
  }
}

Widget paymentAppBarWidget() {
  return AppBar(
      backgroundColor: Colors.deepOrange,
      title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [Text("Foototh", style: appBarTitleTextStyle())]));
}

Widget paymentBodyWidget(context, hotelName, tableNumber, grandTotal) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
          child: _topWidget(hotelName, tableNumber, grandTotal),
        ),
        const MySeparator(),
        _paymentOptions(context, grandTotal),
      ],
    ),
  );
}

Widget _topWidget(hotelName, tableNumber, grandTotal) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text("$hotelName", style: paymentPageTitleTextStyle()),
      _tableNumberWidget(tableNumber),
      Spacer(),
      _grandTotalWidget(grandTotal),
    ],
  );
}

Widget _tableNumberWidget(tableNumber) {
  return RichText(
    text: TextSpan(
      text: 'Table No.: ',
      style: paymentPageNormalTextStyle(),
      children: <TextSpan>[
        TextSpan(
            text: tableNumber, style: TextStyle(fontWeight: FontWeight.bold)),
      ],
    ),
  );
}

Widget _grandTotalWidget(grandTotal) {
  return RichText(
    text: TextSpan(
      text: 'Grand Total: ',
      style: paymentPageNormalTextStyle(),
      children: <TextSpan>[
        TextSpan(
            text: "Rs. $grandTotal",
            style: TextStyle(fontWeight: FontWeight.bold)),
      ],
    ),
  );
}

Widget _paymentOptions(context, grandTotal) {
  return Column(
    children: <Widget>[
      _payButton("Counter Payment",
          () => counterPaymentAPI(context, grandTotal), Colors.orange),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          _payButton("e-dheba", () => print("e-dheba"), Colors.greenAccent),
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          _payButton(
              "Enet Payment", () => print("Enet Payment"), Colors.tealAccent),
          _payButton("connectIPS", () => print("connectIPS"), Colors.blue),
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          _payButton("digiPay", () => print("DigiPay"), Colors.yellow),
          _payButton("IME Pay", () => print("IME Pay"), Colors.red),
          _payButton("CellPay", () => print("Cell Pay"), Colors.teal),
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          _payButton("PrabhuPay", () => print("Prabhu ay"), Colors.red),
          _payButton("QPay", () => print("QPay"), Colors.lightBlue),
          _payButton("Khalti", () => print("Khalti"), Colors.purple),
          _payButton("eSewa", () => print("eSewa"), Colors.lightGreen),
        ],
      ),
    ],
  );
}

Widget _payButton(serviceName, apiToCall, color) {
  return RaisedButton(
    child: Text(serviceName),
    elevation: 2,
    color: color,
    hoverColor: Colors.deepOrange,
    focusColor: Colors.deepOrange,
    splashColor: Colors.deepOrange,
    textColor: Colors.white,
    padding: EdgeInsets.all(8),
    colorBrightness: Brightness.light,
    disabledColor: Colors.grey,
    disabledElevation: 0,
    disabledTextColor: Colors.black,
    onPressed: apiToCall,
  );
}
