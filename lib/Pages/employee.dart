import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foototh/CustomWidgets/cooked_food_list.dart';
import 'package:foototh/CustomWidgets/food_order_list.dart';
import 'package:foototh/CustomWidgets/table_bill_list.dart';
import 'package:foototh/Styles/style.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class EmployeePage extends StatelessWidget {
  // WidgetBuilder(context){
  @override
  Widget build(BuildContext context) {
////////////////////Temporary///////////////////////////
    final String employeeName = 'John Doe';
//    final String employeePost = 'Receptionist';
//    final String employeePost = 'Waiter';
    final String employeePost = 'Chef';

    ///
    return Scaffold(
        appBar: _appBarWidget(employeeName, employeePost),
        bottomNavigationBar: _bottomBar(),
        body: (employeePost == 'Chef'
            ? OrderList()
            : employeePost == 'Waiter'
                ? CookedFoodList()
                : employeePost == 'Receptionist'
                    ? BillingList()
                    : Center(child: Text("Sorry, Employee Post is Mismatched"))));
  }
}

Widget _appBarWidget(employeeName, employeePost) {
  return AppBar(
    backgroundColor: Colors.deepOrange,
    title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          _signOutWidget(),
          _titleTextWidget(employeeName),
          _postTextWidget(employeePost)
        ]),
  );
}

Widget _bottomBar() {
  return Container(
      height: 50,
      constraints: BoxConstraints.tightFor(height: 50),
      child: BottomAppBar(
        color: Colors.deepOrangeAccent,
        child: Container(
            padding: EdgeInsets.all(6),
            child: RaisedButton(
              child: Text("Refresh", style: bottomBarButtonStyle()),
//              color: Color.fromRGBO(239, 187, 118, 0.8),
//              splashColor: Colors.amberAccent,
//              focusColor: Colors.amberAccent,
//              hoverColor: Colors.amberAccent,
              color: Colors.green,
              splashColor: Colors.greenAccent,
              focusColor: Colors.greenAccent,
              hoverColor: Colors.greenAccent,
              textColor: Colors.white,
              shape: StadiumBorder(),
              onPressed: () => print("Refresh"),
            )),
      ));
}

Widget _signOutWidget() {
  return Transform.rotate(
    angle: 3.2,
    child: IconButton(
      icon: new Icon(MdiIcons.logout),
      splashColor: Colors.white12,
      color: Colors.white,
      tooltip: 'Sign Out',
      iconSize: 35,
      onPressed: () => print("Pressed"),
    ),
  );
}

Widget _titleTextWidget(employeeName) {
  return Expanded(
    child: Text(
      employeeName,
      style: appBarTitleTextStyle(),
    ),
  );
}

Widget _postTextWidget(employeePost) {
  return Container(
    padding: const EdgeInsets.all(10.0),
    decoration: BoxDecoration(
        shape: BoxShape.rectangle, color: Color.fromRGBO(38, 27, 32, 0.8)),
    child: Text(
      "$employeePost",
    ),
  );
}
