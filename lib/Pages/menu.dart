import 'package:flutter/material.dart';
import 'package:foototh/CustomWidgets/dropup.dart';
import 'package:foototh/CustomWidgets/food_list.dart';
import 'package:foototh/CustomWidgets/order_drawer.dart';
import 'package:foototh/Styles/style.dart';

class MenuPage extends StatelessWidget {
  // WidgetBuilder(context){
  @override
  Widget build(BuildContext context) {
////////////////////Temporary///////////////////////////
    final String hotelName = 'Hotel';
    final String tableNumber = '9';

    ///
    return Scaffold(
      drawer: OrderedFoodDrawer(hotelName, tableNumber),
      appBar: appBarWidget(hotelName, tableNumber),
      bottomNavigationBar: bottomBar(),
      body: FoodMenuList(),
    );
  }
}

Widget appBarWidget(hotelName, tableNumber) {
  return AppBar(
      backgroundColor: Colors.deepOrange,
      title: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Text("$hotelName's Menu", style: appBarTitleTextStyle(),),
        Container(child: tableNumberWidget(tableNumber))
      ]));
}

Widget tableNumberWidget(tableNumber) {
  return Container(
    padding: const EdgeInsets.all(16.0),
    decoration: BoxDecoration(
        border: Border.all(color: Colors.black),
        shape: BoxShape.circle,
        color: Colors.black),
    child: Text(
      "$tableNumber",
    ),
  );
}

Widget bottomBar() {
  return Container(
      height: 50,
      constraints: BoxConstraints.tightFor(height: 50),
      child: BottomAppBar(
        color: Colors.deepOrangeAccent,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              "Sort Food By Category:",
              style: sortWidgetLabelStyle(),
            ),
            DropUpWidgetForSortingFood('Category')
          ],
        ),
      ));
}
