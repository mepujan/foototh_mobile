import 'package:flutter/material.dart';

void counterPaymentAPI(context,grandTotal) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: Center(child: Text("Counter Payment")),
        content: Container(
          height: 50,
          child: Column(
            children: <Widget>[
              Text("You need to pay Rs. $grandTotal"),
              Spacer(),
              Text("Proceed to counter for payment"),
            ],
          ),
        ),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          FlatButton(
            child: new Text("Ok"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}