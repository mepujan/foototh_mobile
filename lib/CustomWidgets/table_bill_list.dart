import 'package:flutter/material.dart';
import 'package:foototh/Styles/style.dart';

/////////////////////Temporary////////////////
class BillItem {
  const BillItem(this.id, this.tableNumber, this.name, this.total);
  final int id;
  final String name;
  final int tableNumber;
  final double total;
}

///

class BillingList extends StatefulWidget {
  const BillingList();

  State createState() => BillingListState();
}

class BillingListState extends State<BillingList> {
  List<BillItem> _billList = <BillItem>[
    const BillItem(01, 9, 'Kshitiz_s', 1999),
    const BillItem(04, 6, 'Pujan', 300),
    const BillItem(06, 3, '9841702185', 12000),
    const BillItem(07, 1, '9818333107', 5000),
    const BillItem(16, 2, 'Gourav Kadariya', 2000),
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: const EdgeInsets.all(8),
      itemCount: _billList.length,
      itemBuilder: (BuildContext context, int index) {
        return billItem(_billList, index, context);
      },
      separatorBuilder: (BuildContext context, int index) => const Divider(),
    );
  }
}

Container billItem(item, index, _listContext) {
  return Container(
      height: 80,
      color: Colors.amber,
      child: Row(
        // crossAxisAlignment: CrossAxisAlignment.center,
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          _tableNumberWidget(item[index].tableNumber),
          _centerContainer(item[index].name, item[index].total, item[index].id,
              _listContext),
        ],
      ));
}

Widget _centerContainer(name, quantity, id, _listContext) {
  return Expanded(
    child: Container(
        padding: EdgeInsets.all(5),
        child: Column(
//          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(child: _nameAndTotalContainer(name, quantity)),
            _buttonContainer(id, _listContext)
          ],
        )),
  );
}

Widget _buttonContainer(id, _listContext) {
  return Container(
    child: Expanded(
      child: _clearBillButton(id, _listContext),
    ),
  );
}

Widget _clearBillButton(id, _listContext) {
  return SizedBox.expand(
    child: RaisedButton(
      onPressed: () {
        print(id);
        _confirmationDialog(id, _listContext);
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10)
      ),
      textColor: Colors.white,
      color: Colors.deepOrangeAccent,
      child: const Text('Clear Bill', style: TextStyle(fontSize: 20)),
    ),
  );
}

void _confirmationDialog(id, _listContext) {
  showDialog(
    context: _listContext,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: new Text("Bill Cleared"),
        content: Text(id.toString()),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            child: new Text("Close"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

Widget _tableNumberWidget(tableNumber) {
  return Container(
    margin: EdgeInsets.only(left: 5),
    padding: const EdgeInsets.all(10.0),
    decoration: BoxDecoration(
        border: Border.all(color: Colors.black),
        shape: BoxShape.circle,
        color: Colors.black),
    child: Text(
      "$tableNumber",
      style: TextStyle(color: Colors.white, fontSize: 20),
    ),
  );
}

Widget _nameAndTotalContainer(name, quantity) {
  return Row(
    children: <Widget>[
      Expanded(
        child: Text(
          name,
          style: listItemProductNameStyle2(),
        ),
      ),
      Text(
        "Rs. $quantity",
        style: listItemQuantityStyle(),
      )
    ],
  );
}
