import 'package:flutter/material.dart';
import 'package:foototh/Styles/style.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:foototh/Pages/payment.dart';

/////////////////////Temporary////////////////
class OrderedItem {
  const OrderedItem(this.id, this.name, this.quantity, this.price);
  final int id;
  final String name;
  final int quantity;
  final double price;
}

///

class OrderedFoodDrawer extends StatefulWidget {
  final String hotelName;
  final String tableNumber;
  static String grandTotal;
  const OrderedFoodDrawer(this.hotelName, this.tableNumber);

  State createState() => OrderedFoodDrawerState();
}

class OrderedFoodDrawerState extends State<OrderedFoodDrawer> {
  List<OrderedItem> _orders = <OrderedItem>[
    const OrderedItem(01, "Chicken Steam Mo:Mo", 3, 99.99),
    const OrderedItem(07, 'Veg Chowmein', 2, 99.99),
    const OrderedItem(08, 'Chicken Chowmein kkkkkkkkkkkkkkkkkkk', 2, 120),
    const OrderedItem(12, 'Buff Steam Mo:Mo', 4, 110),
    const OrderedItem(13, 'Buff Fried Mo:Mo', 3, 119.99),
    const OrderedItem(01, "Chicken Steam Mo:Mo", 3, 99.99),
    const OrderedItem(08, 'Chicken Chowmein kkkkkkkkkkkkkkkkkkk', 2, 120),
    const OrderedItem(12, 'Buff Steam Mo:Mo', 4, 110),
    const OrderedItem(13, 'Buff Fried Mo:Mo', 3, 119.99),
    const OrderedItem(12, 'Buff Steam Mo:Mo', 4, 110),
    const OrderedItem(13, 'Buff Fried Mo:Mo', 3, 119.99),
    const OrderedItem(01, "Chicken Steam Mo:Mo", 3, 99.99),
    const OrderedItem(08, 'Chicken Chowmein kkkkkkkkkkkkkkkkkkk', 2, 120),
  ];

  @override
  Widget build(BuildContext context) {
    _calcGrandTotal(_orders);
    print("Total = ${OrderedFoodDrawer.grandTotal}");
    return Drawer(
        elevation: 5,
        child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: <Widget>[
                topWidget(context, widget.hotelName, widget.tableNumber),
                dividerWidget(),
                Text(
                  "Order Detail",
                  style: drawerTitleTextStyle(),
                ),
                Expanded(
                  child: ListView.separated(
                    padding: const EdgeInsets.all(1),
                    itemCount: _orders.length,
                    itemBuilder: (BuildContext context, int index) {
                      return _foodItem(_orders, index, context);
                    },
                    separatorBuilder: (BuildContext context, int index) =>
                        const Divider(),
                  ),
                ),
                Divider(
                  color: Colors.deepOrangeAccent,
                  thickness: 1,
                ),
                _grandTotalWidget(_orders),
                dividerWidget(),
                bottomWidget(context, widget.hotelName, widget.tableNumber, OrderedFoodDrawer.grandTotal)
              ],
            )));
  }
}

Container _foodItem(item, index, _listContext) {
  var _totalPrice = item[index].price * item[index].quantity;
  _totalPrice = _totalPrice.toStringAsFixed(2);
  return Container(
      height: 35,
      color: Colors.amber,
      child: Row(
        // crossAxisAlignment: CrossAxisAlignment.center,
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            flex: 4,
            child: Padding(
              padding: const EdgeInsets.only(left: 4),
              child: Text(item[index].name),
            ),
          ),
          Expanded(
              flex: 1, child: Text('X ${item[index].quantity.toString()}')),
          Padding(
            padding: const EdgeInsets.only(right: 4),
            child: Text(
              "Rs. $_totalPrice",
              style: drawerTotalPriceStyle(),
            ),
          )
        ],
      ));
}

Widget _grandTotalWidget(_orders) {
  return Row(
    children: <Widget>[
      Expanded(
        child: Text(
          "Grand Total",
          style: drawerGrandTotalTextStyle(),
        ),
      ),
      Text(
        "Rs. ${OrderedFoodDrawer.grandTotal}",
        style: drawerGrandTotalStyle(),
        textDirection: TextDirection.rtl,
      )
    ],
  );
}

void _calcGrandTotal(_orders) {
  double grandTotal = 0.0;
  for (int i = 0; i < _orders.length; i++) {
    final itemPrice =
        num.parse((_orders[i].price * _orders[i].quantity).toStringAsFixed(2));
    grandTotal += itemPrice;
  }
  OrderedFoodDrawer.grandTotal = grandTotal.toStringAsFixed(2);
}

//ToDo: After DB connection is complete, check if GrandTotal is calculated every time that item changes or it doesn't update at all

Widget bottomWidget(context, hotelName, tableNumber, grandTotal) {
  return Container(
    height: 40,
    child: SizedBox.expand(
      child: RaisedButton(
        child: Text(
          'Confirm Order',
          style: drawerButtonTextStyle(),
        ),
        color: Colors.green,
        hoverColor: Colors.greenAccent,
        focusColor: Colors.greenAccent,
        splashColor: Colors.greenAccent,
        onPressed: () => Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    PaymentPage(hotelName, tableNumber, grandTotal))),
      ),
    ),
  );
}

Widget topWidget(context, hotelName, tableNumber) {
  return SizedBox(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(hotelName, style: drawerTitleTextStyle()),
        Row(
          children: <Widget>[
            Expanded(
              child: _tableNumberWidget(context, tableNumber),
            ),
            languageSelectorWidget(),
            _signOutWidget(),
          ],
        )
      ],
    ),
  );
}

Widget _tableNumberWidget(context, tableNumber) {
  return RichText(
    text: TextSpan(
      text: 'Table No.: ',
      style: drawerNormalTextStyle(),
      children: <TextSpan>[
        TextSpan(
            text: tableNumber, style: TextStyle(fontWeight: FontWeight.bold)),
      ],
    ),
  );
}

Widget languageSelectorWidget() {
  return Text("Language");
}

Widget _signOutWidget() {
  return IconButton(
    icon: new Icon(MdiIcons.logout),
    splashColor: Colors.deepOrangeAccent,
    color: Colors.deepOrange,
    tooltip: 'Sign Out',
    iconSize: 35,
    onPressed: () => print("Sign Out"),
  );
}
