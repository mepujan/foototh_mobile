import 'package:flutter/material.dart';
import 'package:foototh/Styles/style.dart';

class AdminDrawer extends StatelessWidget {
  const AdminDrawer(this.adminName,this.adminMobile,this.hotelPhone,this.hotelAddress,this.hotelName);

  final String adminName;
  final String adminMobile;
  final String hotelName;
  final String hotelAddress;
  final String hotelPhone;
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Spacer(),
          _topPart(adminName, adminMobile),
          Expanded(child: _middlePart(hotelName, hotelAddress, hotelPhone)),
          _signOutButton(),
        ],
      ),
    );
  }
}

Widget _topPart(adminName, adminMobile) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text("Name: $adminName",style: adminDrawerNormalTextStyle(),),
      Text("Mobile No.: $adminMobile",style: adminDrawerNormalTextStyle(),),
      MySeparator(),
    ],
  );
}

Widget _middlePart(hotelName, hotelAddress, hotelPhone) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(hotelName,style: adminDrawerNormalTextStyle(),),
      Text(hotelAddress,style: adminDrawerNormalTextStyle()),
      Text(hotelPhone,style: adminDrawerNormalTextStyle()),
      MySeparator(),
    ],
  );
}

Widget _signOutButton() {
  return FlatButton(
      onPressed: () => print("Sign Out0"),
      child: Text('Sign Out',style: adminDrawerNormalTextStyle())
  );
}
