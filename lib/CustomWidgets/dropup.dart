import 'package:flutter/material.dart';
import 'package:foototh/Styles/style.dart';

/////////////////////Temporary////////////////
class Item {
  const Item(this.name);
  final String name;
}
///

class DropUpWidgetForSortingFood extends StatefulWidget {
  final String hintText;
  const DropUpWidgetForSortingFood(this.hintText);

  State createState() => DropUpWidgetForSortingFoodState();
}

class DropUpWidgetForSortingFoodState
    extends State<DropUpWidgetForSortingFood> {
  Item selectedCategory;
///////////////////////////////
  List<Item> _categories = <Item>[
    const Item('Mo:Mo'),
    const Item('Veg'),
    const Item('Non-Veg'),
    const Item('Sea Food'),
    const Item('Continental'),
    const Item('Newari'),
    const Item('Chinese'),
    const Item('Indian'),
    const Item('Western'),
    const Item('Thakali'),
    const Item('Appetizers'),
    const Item('Salads'),
    const Item('Entrées'),
    const Item('Side Items'),
    const Item('Beverages'),
    const Item('Ice Cream'),
    const Item('Cuban'),
    const Item('Italian'),
    const Item('Polish'),
    const Item('Thai'),
  ];
/////////
  @override
  Widget build(BuildContext context) {
    return Center(
      child: DropdownButton<Item>(
        hint: Text('Select ${widget.hintText}', style: dropUpHintStyle()),
        value: selectedCategory,
        style: sortOptionsStyle(),
        icon: Icon(Icons.arrow_drop_up),
        onChanged: (Item value) {
          setState(() {
            selectedCategory = value;
          });
        },
        items: _categories.map((Item category) {
          return DropdownMenuItem<Item>(
            value: category,
            child: dropUpItems(category),
          );
        }).toList(),
      ),
    );
  }
}

Widget dropUpItems(category) {
  return Row(
    children: <Widget>[
      SizedBox(
        width: 10,
      ),
      Text(
        category.name,
        style: TextStyle(color: Colors.black),
      ),
    ],
  );
}

DropdownMenuItem<String> tempo(_categories){
  return _categories.map((Item category) {
          return DropdownMenuItem<Item>(
            value: category,
            child: dropUpItems(category),
          );
        }).toList();
}
