import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
/////////////////////Temporary////////////////
// class EmployeeItem {
//   const EmployeeItem(this.id, this.name, this.post, this.imageURL);
//   final int id;
//   final String name;
//   final String post;
//   final String imageURL;
// }
  // Future<EmployeeModel> fetchEmployee() async {
  // final response = await http.get("http://10.0.3.2:8000/employee/");

  // if (response.statusCode == 200) {
  //   // If the server did return a 200 OK response, then parse the JSON.
  //   return EmployeeModel.fromJson(json.decode(response.body);
  // } else {
  //   // If the server did not return a 200 OK response, then throw an exception.
  //   throw Exception('Failed to load album');
  // }
// }
///

class EmployeeList extends StatefulWidget {

  State createState() => EmployeeListState();
}

class EmployeeListState extends State<EmployeeList> {

List data;
  Future<List> _getJsonData() async{
    final response = await http.get(
      Uri.encodeFull("http://10.0.3.2:8000/employee/"),
      headers: {'Accept':'application/json'},
      
    );
    
   return json.decode(response.body);  

     
  }

  // List<EmployeeItem> _employeeList = <EmployeeItem>[
  //   const EmployeeItem(01, 'Kshitiz_s Sharma', 'Admin', 'images/test_img2.JPG'),
  //   const EmployeeItem(02, 'Gourav Kadariya', 'Chef', 'images/test_img1.jpg'),
  //   const EmployeeItem(03, 'Pujan Gautam', 'Manager', 'images/test_img3.JPG'),
  //   const EmployeeItem(04, 'Pujan Gautam', 'Chef', 'images/test_img1.jpg'),
  //   const EmployeeItem(08, 'John Doe', 'Waiter', 'images/test_img2.JPG'),
  //   const EmployeeItem(09, 'Jane Doe', 'Receptionist', 'images/test_img2.JPG'),
  //   const EmployeeItem(05, 'Robin', 'Receptionist', 'images/test_img2.JPG'),
  //   const EmployeeItem(06, 'Bobin', 'Waiter', 'images/test_img1.jpg'),
  //   const EmployeeItem(07, 'Jubin', 'Waiter', 'images/test_img3.JPG'),
  //   const EmployeeItem(10, 'Johnny Doe', 'Chef', 'images/test_img3.JPG'),
  //   const EmployeeItem(11, 'John Roe', 'Waiter', 'images/test_img1.jpg'),
  //   const EmployeeItem(12, 'Richard Roe', 'Accountant', 'images/test_img3.JPG'),
  //   const EmployeeItem(13, 'Janie Doe', 'Receptionist', 'images/test_img2.JPG'),
  //   const EmployeeItem(14, 'Baby Doe', 'Waiter', 'images/test_img3.JPG'),
  //   const EmployeeItem(15, 'Jane Roe', 'Chef', 'images/test_img1.jpg'),
  // ];

  // Future<EmployeeModel> futureEmployee;

  // @override

  // void initState() {
  //   super.initState();
  //   futureEmployee = fetchEmployee();
  // }
@override
Widget build(BuildContext context) {
return MaterialApp(
      home: Scaffold(
          body:new FutureBuilder<List>(
        future: _getJsonData(),
        builder: (context,snapshot){
          if(snapshot.hasError) print(snapshot.error);
          return snapshot.hasData ? new ItemList(snapshot.data):new Center(
            child: new CircularProgressIndicator(),
          );
        },
      ),
    //     body: Center(
    //       child: FutureBuilder<EmployeeModel>(
    //         future: futureEmployee,
    //         builder: (context, snapshot) {
    //           if (snapshot.hasData) {
    //             return Text(snapshot.data.name);
    //           } else if (snapshot.hasError) {
    //             return Text("${snapshot.error}");
    //           }

    //           // By default, show a loading spinner.
    //           return CircularProgressIndicator();
    //         },
    //       ),
    //     ),
      ),
    );
  }

//     return ListView.separated(
//       padding: const EdgeInsets.all(8),
//       itemCount: _employeeList.length,
//       itemBuilder: (BuildContext context, int index) {
//         return employee(_employeeList, index);
//       },
//       separatorBuilder: (BuildContext context, int index) => const Divider(),
//     );
//   }
}

// Container employee(item, index) {
//   return Container(
//       height: 80,
// //      color: Colors.amber,
//       child: RaisedButton(
//         padding: EdgeInsets.all(2),
//         onPressed: () => print(item[index].name),
//         color: Colors.amber,
//         hoverColor: Colors.amberAccent,
//         focusColor: Colors.amberAccent,
//         splashColor: Colors.amberAccent,
//         child: Row(
//           // crossAxisAlignment: CrossAxisAlignment.center,
//           // mainAxisAlignment: MainAxisAlignment.spaceBetween,
//           children: <Widget>[
//              imageContainer(item[index].imageURL),
//             centerContainer(item[index].name, item[index].post),
//           ],
//         ),
//       )
//   );
// }

// Widget imageContainer(url) {
//   return Container(
// //      decoration: BoxDecoration(
// //        border: Border.all(color: Colors.black),
// //        borderRadius: const BorderRadius.all(const Radius.circular(40))
// //      ),
//       alignment: Alignment.centerLeft,
// //      padding: EdgeInsets.all(2),
//       child: CircleAvatar(
//         backgroundImage: NetworkImage('https://i.picsum.photos/id/1027/2848/4272.jpg'),
//         radius: 45,
//       )
//       );
// }

// Widget centerContainer(name, post) {
//   return Expanded(
//     child: Container(
//         // decoration: BoxDecoration(
//         //   border: Border.all(color: Colors.black),
//         // ),
//         padding: EdgeInsets.all(5),
//         child: Column(
//           // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: <Widget>[
//             Expanded(
//               child: Text(
//                 name,
//                 style: listItemProductNameStyle(),
//               ),
//             ),
//             Text(
//               'Post: $post',
//               style: listItemCategoryStyle(),
//             )
//           ],
//         )),
//   );
// }
class ItemList extends StatelessWidget {
  final List list;
  ItemList(this.list);
  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
      itemCount: list==null ? 0 : list.length,
      itemBuilder:(context, i){
        return new Text(list[i]['name']);
      } ,
      
    );
  }
}