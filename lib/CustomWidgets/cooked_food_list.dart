import 'package:flutter/material.dart';
import 'package:foototh/Styles/style.dart';

/////////////////////Temporary////////////////
class CookedItem {
  const CookedItem(this.id, this.tableNumber, this.name, this.quantity);
  final int id;
  final String name;
  final int tableNumber;
  final int quantity;
}

///

class CookedFoodList extends StatefulWidget {
  const CookedFoodList();

  State createState() => CookedFoodListState();
}

class CookedFoodListState extends State<CookedFoodList> {
  List<CookedItem> _cookedList = <CookedItem>[
    const CookedItem(01, 9, 'Chicken Steam Mo:Mo',2),
    const CookedItem(02, 9, 'Buff Steam Mo:Mo',3),
    const CookedItem(03, 9, 'Ham Burger',2),
    const CookedItem(04, 6, 'Buff Fried kk kkkkkkkkkkkkk Mo:Mo',2),
    const CookedItem(05, 6, 'Espresso',2),
    const CookedItem(06, 3, 'Chicken Kabab',1),
    const CookedItem(07, 1, 'Veg. Chowmein',2),
    const CookedItem(08, 1, 'Coca-Cola',2),
    const CookedItem(09, 9, 'Chicken Steam Mo:Mo',2),
    const CookedItem(10, 9, 'Chicken Steam Mo:Mo',2),
    const CookedItem(11, 9, 'Chicken Steam Mo:Mo',2),
    const CookedItem(12, 9, 'Chicken Steam Mo:Mo',2),
    const CookedItem(13, 9, 'Chicken Steam Mo:Mo',2),
    const CookedItem(14, 9, 'Chicken Steam Mo:Mo',2),
    const CookedItem(15, 9, 'Chicken Steam Mo:Mo',2),
    const CookedItem(16, 9, 'Chicken Steam Mo:Mo',2),
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: const EdgeInsets.all(8),
      itemCount: _cookedList.length,
      itemBuilder: (BuildContext context, int index) {
        return cookedItem(_cookedList, index, context);
      },
      separatorBuilder: (BuildContext context, int index) => const Divider(),
    );
  }
}

Container cookedItem(item, index, _listContext) {
  return Container(
      height: 80,
      color: Colors.amber,
      child: Row(
        // crossAxisAlignment: CrossAxisAlignment.center,
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          tableNumberWidget(item[index].tableNumber),
          _centerContainer(item[index].name,item[index].quantity),
          _buttonContainer(item[index].id, _listContext),
        ],
      ));
}

Widget _centerContainer(name,quantity) {
  return Expanded(
    child: Container(
      // decoration: BoxDecoration(
      //   border: Border.all(color: Colors.black),
      // ),
        padding: EdgeInsets.all(5),
        child: Column(
//          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Text(
                name,
                style: listItemProductNameStyle2(),
              ),
            ),
            Text(
              'Quantity: $quantity',
              style: listItemQuantityStyle(),
            )
          ],
        )),
  );
}

Widget _buttonContainer(id, _listContext) {
  return Container(
      padding: EdgeInsets.symmetric(horizontal: 5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _deliveredButton(id, _listContext),
        ],
      ));
}

Widget _deliveredButton(id, _listContext) {
  return RaisedButton(
    onPressed: () {
      print(id);
      _confirmationDialog(id, _listContext);
    },
    textColor: Colors.white,
    color: Colors.deepOrangeAccent,
    child: const Text('Delivered', style: TextStyle(fontSize: 20)),
  );
}

void _confirmationDialog(id, _listContext) {
  showDialog(
    context: _listContext,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: new Text("Food Item Delivered"),
        content: Text(id.toString()),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            child: new Text("Close"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

Widget tableNumberWidget(tableNumber) {
  return Container(
    margin: EdgeInsets.only(left: 5),
    padding: const EdgeInsets.all(10.0),
    decoration: BoxDecoration(
        border: Border.all(color: Colors.black),
        shape: BoxShape.circle,
        color: Colors.black),
    child: Text(
      "$tableNumber", style: TextStyle(color: Colors.white, fontSize: 20),
    ),
  );
}