import 'package:flutter/material.dart';
import 'package:foototh/Styles/style.dart';
// import 'package:http/http.dart' as http;
// import 'dart:async';
// import 'dart:convert';

/////////////////////Temporary////////////////
class MenuItem {
  const MenuItem(this.id, this.name, this.category, this.price);
  final int id;
  final String name;
  final String category;
  final double price;
}

///

class FoodMenuList extends StatefulWidget {
  const FoodMenuList();

  State createState() => FoodMenuListState();
}

class FoodMenuListState extends State<FoodMenuList> {

// Future<List> _getJsonData() async{
//     final response = await http.get(
//       Uri.encodeFull("http://10.0.2.2:8000/foodList/"),
//       headers: {'Accept':'application/json'},
      
//     );
    
//    return json.decode(response.body);  

     
//   }


  List<MenuItem> _menuList = <MenuItem>[
    const MenuItem(01, 'Chicken Steam Mo:Mo', 'Mo:Mo', 99.99),
    const MenuItem(02, 'Chicken Fried Mo:Mo', 'Mo:Mo', 140),
    const MenuItem(03, 'Buff Steam Mo:Mo (Super Special)', 'Mo:Mo', 1100.99),
    const MenuItem(04, 'Buff Fried Mo:Mo', 'Mo:Mo', 11119.99),
    const MenuItem(05, 'Chicken Fried Mo:Mo', 'Mo:Mo', 140),
    const MenuItem(06, 'Buff Steam Mo:Mo', 'Mo:Mo', 110),
    const MenuItem(07, 'Veg Chowmein', 'Chowmein', 99.99),
    const MenuItem(08, 'Chicken Chowmein', 'Chowmein', 120),
    const MenuItem(09, 'Buff Chowmein', 'Chowmein', 110),
    const MenuItem(10, 'Buff Fried Mo:Mo', 'Mo:Mo', 119.99),
    const MenuItem(11, 'Chicken Fried Mo:Mo', 'Mo:Mo', 140),
    const MenuItem(12, 'Buff Steam Mo:Mo', 'Mo:Mo', 110),
    const MenuItem(13, 'Buff Fried Mo:Mo', 'Mo:Mo', 119.99),
    const MenuItem(14, 'Chicken Fried Mo:Mo', 'Mo:Mo', 140),
    const MenuItem(15, 'Buff Steam Mo:Mo', 'Mo:Mo', 110),
    const MenuItem(16, 'Buff Fried Mo:Mo', 'Mo:Mo', 119.99),
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: const EdgeInsets.all(8),
      itemCount: _menuList.length,
      itemBuilder: (BuildContext context, int index) {
        return foodItem(_menuList, index, context);
      },
      separatorBuilder: (BuildContext context, int index) => const Divider(),
    );
  }
}

Container foodItem(item, index, _listContext) {
  return Container(
      height: 80,
      color: Colors.amber,
      child: Row(
        // crossAxisAlignment: CrossAxisAlignment.center,
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          centerContainer(item[index].name, item[index].category),
          priceAndOrderContainer(_listContext, item[index].id,
              item[index].price, item[index].name),
        ],
      ));
}

Widget centerContainer(name, category) {
  return Expanded(
    child: Container(
        // decoration: BoxDecoration(
        //   border: Border.all(color: Colors.black),
        // ),
        padding: EdgeInsets.all(5),
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Text(
                name,
                style: listItemProductNameStyle(),
              ),
            ),
            Text(
              'Category: $category',
              style: listItemCategoryStyle(),
            )
          ],
        )),
  );
}

Widget priceAndOrderContainer(_listContext, id, price, foodName) {
  return Container(
      // decoration: BoxDecoration(
      //   border: Border.all(color: Colors.black),
      // ),
      padding: EdgeInsets.symmetric(horizontal: 5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          priceWidget(price),
          orderButton(_listContext, id, foodName),
        ],
      ));
}

Widget priceWidget(price) {
  return Container(
    // decoration: BoxDecoration(
    //   border: Border.all(color: Colors.black),
    // ),
    padding: EdgeInsets.symmetric(vertical: 2),
    child: Text(
      'Rs. ${price.toString()}',
      style: foodItemPriceStyle(),
      textDirection: TextDirection.rtl,
    ),
  );
}

Widget orderButton(_listContext, id, foodName) {
  return RaisedButton(
    onPressed: () {
      _orderConfirmationDialog(_listContext, id, foodName);
    },
    textColor: Colors.white,
    color: Colors.deepOrangeAccent,
    child: const Text('Order', style: TextStyle(fontSize: 20)),
  );
}

void _orderConfirmationDialog(_listContext, id, foodName) {
  showDialog(
    context: _listContext,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: _popUpTitle(foodName),
        content: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Quantity: ", style: popUpNormalTextStyle()),
          
          ],
        ),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            child: new Text("Add Order"),
            onPressed: () {
              print(id);
              Navigator.of(context).pop();
            },
          ),
          new FlatButton(
            child: new Text("Cancel"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

Widget _popUpTitle(foodName) {
  return RichText(
    softWrap: false,
    overflow: TextOverflow.ellipsis,
    maxLines: 3,
    textAlign: TextAlign.center,
    text: TextSpan(
      text:
          'Order For                ', //Yo blank space haru chainxa, if you want to ensure that next text is drawn on new line
      style: popUpTitleTextStyle(),
      children: <TextSpan>[
        TextSpan(text: foodName, style: TextStyle(fontWeight: FontWeight.bold)),
      ],
    ),
  );
}
