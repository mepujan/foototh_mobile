import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:foototh/Styles/style.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:async';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'admin_drawer.dart';

class AddEmployee extends StatefulWidget {
  @override
  _AddEmployeeState createState() => _AddEmployeeState();
}

int _selectedPost;

List<DropdownMenuItem<int>> postList = [];

////////////////////Temporary///////////////////////////
final int hotelId=1;
final String hotelName = 'Hotel';
final String hotelPhone = '015598765';
final String hotelAddress = 'Mid-Baneshwor, Lalitpur';
final String adminName = 'John Doe';
final String adminMobile = '9841234567';

///

class _AddEmployeeState extends State<AddEmployee> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController phoneNumberController = TextEditingController();
  TextEditingController mobileNumberController = TextEditingController();
  TextEditingController postController = TextEditingController();


  File _image;

  Future getImageFromGallery() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = image;
    });
  }

  Future getImageFromCamera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      _image = image;
    });
  }

  @override
  Widget build(BuildContext context) {
    loadPostList();
    return Scaffold(
        appBar: AppBar(
          title: Text("Add New Employee"),
        ),
        endDrawer: AdminDrawer(
            adminName, adminMobile, hotelPhone, hotelAddress, hotelName),
        body: Builder(
            builder: (context) => Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      Expanded(
                        child: ListView(
//                    padding: const EdgeInsets.all(30.0),
//                        crossAxisAlignment: CrossAxisAlignment.stretch,
//                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              _textFieldPadding(
                                //Full Name
                                TextFormField(
                                  controller: nameController,
                                  decoration: beautifulField('Full Name *'),
                                  validator: (value) {
                                    return value.isEmpty
                                        ? "Employee Name can't be empty"
                                        : null;
                                  },
                                  keyboardType: TextInputType.text,
                                 
                                ),
                              ),
                              _textFieldPadding(
                                //Address
                                TextFormField(
                                  controller: addressController,
                                  decoration: beautifulField('Address *'),
                                  validator: (value) {
                                    return value.isEmpty
                                        ? "Employee Address can't be empty"
                                        : null;
                                  },
                                  keyboardType: TextInputType.text,
                                  
                                ),
                              ),
                              _textFieldPadding(
                                //Mobile Number
                                TextFormField(
                                  controller: mobileNumberController,
                                  decoration: beautifulField('Mobile Number *'),
                                  validator: (value) {
                                    return value.isEmpty
                                        ? "Mobile Number can't be empty"
                                        : (value.length != 10)
                                            ? "Mobile Number should of of length 10"
                                            : (!value.startsWith('98'))
                                                ? "Mobile Number must start with 98"
                                                : null;
                                  },
                                  keyboardType: TextInputType.phone,
                                  inputFormatters: <TextInputFormatter>[
                                    WhitelistingTextInputFormatter(
                                        RegExp(r"[\d]")),
                                    LengthLimitingTextInputFormatter(10),
                                  ],
                                 
                                ),
                              ),
                              _textFieldPadding(
                                //Phone Number
                                TextFormField(
                                  controller: phoneNumberController,
                                  decoration: beautifulField('Phone Number'),
                                  validator: (value) {
                                    return (value.length == 0)
                                        ? null
                                        : (value.length != 9)
                                            ? "Mobile Number should of of length 9"
                                            : null;
                                  },
                                  keyboardType: TextInputType.phone,
                                  inputFormatters: <TextInputFormatter>[
                                    WhitelistingTextInputFormatter(
                                        RegExp(r"[\d]")),
                                    LengthLimitingTextInputFormatter(9),
                                  ],
                                ),
                              ),
                              _textFieldPadding(
                                //Post
                                DropdownButtonFormField(
                                  decoration: beautifulField('Post *'),
                                  items: postList,
                                  value: _selectedPost,
                                  onChanged: (value) {
                                    setState(() {
                                      _selectedPost = value;
                                     
                                    });
                                  },
                                  isDense: true,
                                  validator: (value) {
                                    return (value==null)
                                        ? 'Please select Employee Post'
                                        : null;
                                  },
                                ),
                              ),
                              _textFieldPadding(
                                //Image
                                Container(
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Colors.grey, width: 1),
                                      borderRadius: new BorderRadius.all(
                                          const Radius.circular(25.0))),
                                  child: ListTile(
                                    title: Row(
                                      children: <Widget>[
                                        Expanded(
                                            child: Text(
                                          'Photo *',
                                          style:
                                              addEmployeeFormLabelTextStyle(),
                                        )),
                                        RaisedButton(
                                          onPressed: getImageFromGallery,
                                          focusColor: Colors.deepOrangeAccent,
                                          highlightColor: Colors.deepOrangeAccent,
                                          splashColor: Colors.deepOrangeAccent,
                                          hoverColor: Colors.deepOrangeAccent,
                                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                                          child: Icon(Icons.photo),
                                        ),
                                        Padding(padding: EdgeInsets.symmetric(horizontal: 5),),
                                        RaisedButton(
                                          onPressed: getImageFromCamera,
                                          focusColor: Colors.deepOrangeAccent,
                                          highlightColor: Colors.deepOrangeAccent,
                                          splashColor: Colors.deepOrangeAccent,
                                          hoverColor: Colors.deepOrangeAccent,
                                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                                          child: Icon(Icons.camera),

                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),

// //
                              // Center(
                              //   child: _image == null
                              //       ? Text('No image selected.')
                              //       : Image.file(_image),
                              // ),
                              Padding(
                                //Bottom padding for the last form element
                                padding: const EdgeInsets.only(top: 30),
                                child: SizedBox(),
                              ),
                            ]),
                      ),
                      Container(
                        color: Colors.deepOrange,
                        height: 70,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            RaisedButton(
                                elevation: 15,
                                color: Colors.green,
                                textColor: Colors.white,
                                hoverColor: Colors.greenAccent,
                                focusColor: Colors.greenAccent,
                                splashColor: Colors.greenAccent,
                                highlightColor: Colors.greenAccent,
                                onPressed: () {
                                  final form = _formKey.currentState;

                                  if (form.validate()) {
                                    setState(() {
                                      _saveEmployee(nameController.text,addressController.text,phoneNumberController.text,mobileNumberController.text,_selectedPost);
                                      debugPrint('Save button is pressed');
                                    });

                                    _showSavedDialog(context);
                                  }
                                },
                                child: Text('Save',
                                    style: TextStyle(fontSize: 20))),
                            RaisedButton(
                                elevation: 15,
                                color: Colors.red,
                                textColor: Colors.white,
                                splashColor: Colors.redAccent,
                                focusColor: Colors.redAccent,
                                highlightColor: Colors.redAccent,
                                hoverColor: Colors.redAccent,
                                onPressed: () {
                                  final form = _formKey.currentState;
                                  form.reset();
                                  _showResetDialog(context);
                                },
                                child: Text('Clear',
                                    style: TextStyle(fontSize: 20))),
                          ],
                        ),
                      )
                    ],
                  ),
                )));
  }

  _showSavedDialog(BuildContext context) {
    Scaffold.of(context)
        .showSnackBar(SnackBar(content: Text('Form Submitted')));
  }

  _showResetDialog(BuildContext context) {
    Scaffold.of(context).showSnackBar(SnackBar(content: Text('Form Cleared')));
  }
}

void _saveEmployee(String name,String address,String phone,String mobile,int post) async{
 
 Map data = {'name': name, 'address': address,'phone_number':phone,'mobile_number':mobile,'post':post.toString(),'hotel': hotelId.toString()};
  var response = await http.post(
        Uri.encodeFull("http://10.0.3.2:8000/employee/"),
        headers: {'Accept': 'application/json'},
        body: data);
    if (response.statusCode == 200) {
      print(response.body);
      json.decode(response.body);
    }else {
      print(response.body);
    }

}

Widget _textFieldPadding(formField) {
  return Padding(
    padding: EdgeInsets.fromLTRB(30, 30, 30, 0),
    child: formField,
  );
}

void loadPostList() {
  postList = [];
  postList.add(new DropdownMenuItem(
    child: new Text('Chef'),
    value: 1,
  ));
  postList.add(new DropdownMenuItem(
    child: new Text('Waiter'),
    value: 2,
  ));
  postList.add(new DropdownMenuItem(
    child: new Text('Receptionist'),
    value: 3,
  ));
}

