import 'package:flutter/material.dart';
import 'package:foototh/Styles/style.dart';

/////////////////////Temporary////////////////
class OrderItem {
  const OrderItem(this.id, this.tableNumber, this.name, this.quantity);
  final int id;
  final String name;
  final int tableNumber;
  final int quantity;
}

///

class OrderList extends StatefulWidget {
  const OrderList();

  State createState() => OrderListState();
}

class OrderListState extends State<OrderList> {
  List<OrderItem> _orderList = <OrderItem>[
    const OrderItem(01, 9, 'Chicken Steam Mo:Mo',2),
    const OrderItem(02, 9, 'Buff Steam Mo:Mo',3),
    const OrderItem(03, 9, 'Ham Burger',2),
    const OrderItem(04, 6, 'Buff Fried kk kkkkkkkkkkkkk Mo:Mo',2),
    const OrderItem(05, 6, 'Espresso',2),
    const OrderItem(06, 3, 'Chicken Kabab',1),
    const OrderItem(07, 1, 'Veg. Chowmein',2),
    const OrderItem(08, 1, 'Coca-Cola',2),
    const OrderItem(09, 9, 'Chicken Steam Mo:Mo',2),
    const OrderItem(10, 9, 'Chicken Steam Mo:Mo',2),
    const OrderItem(11, 9, 'Chicken Steam Mo:Mo',2),
    const OrderItem(12, 9, 'Chicken Steam Mo:Mo',2),
    const OrderItem(13, 9, 'Chicken Steam Mo:Mo',2),
    const OrderItem(14, 9, 'Chicken Steam Mo:Mo',2),
    const OrderItem(15, 9, 'Chicken Steam Mo:Mo',2),
    const OrderItem(16, 9, 'Chicken Steam Mo:Mo',2),
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: const EdgeInsets.all(8),
      itemCount: _orderList.length,
      itemBuilder: (BuildContext context, int index) {
        return orderItem(_orderList, index, context);
      },
      separatorBuilder: (BuildContext context, int index) => const Divider(),
    );
  }
}

Container orderItem(item, index, _listContext) {
  return Container(
      height: 80,
      color: Colors.amber,
      child: Row(
        // crossAxisAlignment: CrossAxisAlignment.center,
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
//          _tableNumberContainer(item[index].tableNumber),
        _tableNumberWidget(item[index].tableNumber),
          _centerContainer(item[index].name,item[index].quantity),
          _buttonContainer(item[index].id, _listContext),
        ],
      ));
}

Widget _centerContainer(name,quantity) {
  return Expanded(
    child: Container(
      // decoration: BoxDecoration(
      //   border: Border.all(color: Colors.black),
      // ),
        padding: EdgeInsets.all(5),
        child: Column(
//          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Text(
                name,
                style: listItemProductNameStyle2(),
              ),
            ),
            Text(
              'Quantity: $quantity',
              style: listItemQuantityStyle(),
            )
          ],
        )),
  );
}

Widget _buttonContainer(id, _listContext) {
  return Container(
      padding: EdgeInsets.symmetric(horizontal: 5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _doneButton(id, _listContext),
        ],
      ));
}

Widget _doneButton(id, _listContext) {
  return RaisedButton(
    onPressed: () {
      print(id);
      _confirmationDialog(id, _listContext);
    },
    textColor: Colors.white,
    color: Colors.deepOrangeAccent,
    child: const Text('Done', style: TextStyle(fontSize: 20)),
  );
}

void _confirmationDialog(id, _listContext) {
  showDialog(
    context: _listContext,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: new Text("Food Item Cooked"),
        content: Text(id.toString()),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            child: new Text("Close"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

Widget _tableNumberWidget(tableNumber) {
  return Container(
    margin: EdgeInsets.only(left: 5),
    padding: const EdgeInsets.all(10.0),
    decoration: BoxDecoration(
        border: Border.all(color: Colors.black),
        shape: BoxShape.circle,
        color: Colors.black),
    child: Text(
      "$tableNumber", style: TextStyle(color: Colors.white, fontSize: 20),
    ),
  );
}